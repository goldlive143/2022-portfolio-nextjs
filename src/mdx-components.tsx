import { ComponentPropsWithRef } from "react";
import { Link, Paragraph } from "@/components";

export function useMDXComponents(components) {
  return {
    ...components,
    h1: (props: ComponentPropsWithRef<'h1'>) => <h1 {...props} className="h1" />,
    h2: (props: ComponentPropsWithRef<'h2'>) => <h2 {...props} className="h2" />,
    h3: (props: ComponentPropsWithRef<'h3'>) => <h3 {...props} className="h3" />,
    a: Link,
    p: Paragraph,
    hr: (props) => (<hr {...props} className="hr" />),
    ul: (props: ComponentPropsWithRef<'ul'>) => <ul {...props} className="ul list" />,
    ol: (props: ComponentPropsWithRef<'ol'>) => <ol {...props} className="ol list" />,
    li: (props: ComponentPropsWithRef<'li'>) => <li {...props} className="li" />,
    img: (props: ComponentPropsWithRef<'img'>) => <img {...props} className="img" />,
    code: (props: ComponentPropsWithRef<'code'>) => <code {...props} className="code" />,
  };
}
