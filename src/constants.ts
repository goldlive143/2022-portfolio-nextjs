export const dateOptions: Intl.DateTimeFormatOptions = {
  year: "numeric",
  month: "long",
  day: "numeric",
};

export const PROJECT_URL = "https://gitlab.com/chadlavimoniere/2022-portfolio-nextjs/-/blob/main";
