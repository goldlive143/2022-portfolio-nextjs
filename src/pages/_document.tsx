import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <link rel="shortcut icon" href="/favicon.png" type="image/x-icon" />
        <meta property="og:image" content="https://www.chadlavimoniere.com/chad-about-me.jpeg" />
        <meta name="description" content="Chad Lavimoniere is a Product Designer currently working at GitLab" />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <body className='body'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}