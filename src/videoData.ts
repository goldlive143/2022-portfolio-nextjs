export interface Video {
  title: string;
  url: string;
  embed: string;
  description?: string;
  date: string;
}

export const videos: Video[] = [
  {
    title:
      "UX Showcase - UNO Reverse: teach me about how traditional designers work at GitLab",
    url: "https://www.youtube.com/watch?v=KfdeVFMdp7I",
    embed:
      "https://www.youtube-nocookie.com/embed/KfdeVFMdp7I?si=40rXLmvmJcf08uRq",
    date: "2024-03-20",
  },
];
