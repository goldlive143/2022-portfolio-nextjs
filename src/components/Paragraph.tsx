import { ComponentPropsWithRef } from "react"

const Paragraph = (props: ComponentPropsWithRef<'p'>) => <p {...props} className="p" />

export default Paragraph
