const ResumeDate = ({children}) => (
  <p className="text-sm !mt-0 italic">{children}</p>
)

export default ResumeDate
