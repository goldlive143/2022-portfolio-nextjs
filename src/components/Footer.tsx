import { Link } from "@/components"
import { PROJECT_URL } from "@/constants"

const Footer = ({ page }: { page?: string }) => (
  <footer className="footer">
    {new Date().getFullYear()} &copy; Chad Lavimoniere {page && <>(<Link href={`${PROJECT_URL}/src/pages/${page}`}>source</Link>)</>}
  </footer>
)

export default Footer
