import pages from "@/pageData"
import Listing from "./Listing";

const Blogs = () => pages
  .filter(p => (p.blog && !p.hidden))
  .map(p => <Listing directory="blog" key={p.href} {...p} />)

export default Blogs;
