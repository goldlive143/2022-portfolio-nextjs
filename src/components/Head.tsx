import { PageProps } from '@/pageData'
import Head from 'next/head'

const HeadComponent = ({ project: {title, snippet} }: { project: Partial<PageProps> }) => (
  <Head>
    {title && <title>{`${title} — Chad Lavimoniere`}</title>}
    {snippet && <meta name="description" content={snippet} />}
  </Head>
)

export default HeadComponent
