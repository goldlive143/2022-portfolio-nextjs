import { ComponentPropsWithRef } from "react"

const Link = (props: ComponentPropsWithRef<'a'>) => (
  <a
    {...props}
    target={props?.href?.match(/^http/) ? "_blank" : undefined}
    className="link"
  />
)

export default Link
