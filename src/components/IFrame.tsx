import { ComponentPropsWithRef } from "react";

const IFrame = (props: ComponentPropsWithRef<'iframe'>) => (
  <iframe
    className="iframe"
    {...props}
  />
)

export default IFrame