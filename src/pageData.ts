export interface PageProps {
  /** Page title */
  title: string;
  /** Page href within the projects dir */
  href: string;
  /** Preview snippet for project pages */
  snippet?: string;
  /** Page is a project page */
  project?: boolean;
  /** Page is a blog page */
  blog?: boolean;
  type?: "blog" | "project";
  hidden?: boolean;
  date?: string;
}

/**
 * Anything added to this array will have `blog: true` appended to it.
 *
 * Blogs will be sorted in the exact order shown here.
 */
const blogs: PageProps[] = [
  {
    title: "My workout routine",
    href: "six-week-shred",
    snippet: "These are the workouts I'm currently doing",
    date: "2024-03-06",
  },
];

const sharedBlogProps: Partial<PageProps> = {
  type: "blog",
  blog: true,
};

/**
 * Anything added to this array will have `project: true` appended to it.
 *
 * Projects will be sorted in the exact order shown here.
 */
const projects: PageProps[] = [
  {
    title: "Lasers & Feelings character sheet",
    href: "lasers-feelings",
    snippet: "a simple Lasers & Feelings character sheet in the browser",
    date: "2024-02-17",
  },
  {
    title: "react-highlighter-ts",
    href: "react-highlighter",
    snippet: "a TypeScript react library for creating dynamic text highlights",
  },
  {
    title: "Recipes",
    href: "recipes",
    snippet:
      "I keep all my recipes in a git repo of markdown files. Frequently updated!",
  },
  {
    title: "Alone among the stars",
    href: "alone-among-the-stars",
    snippet: "A tiny web version of a cool card game",
  },
  {
    title: "Bug prioritizer",
    href: "bug-prioritizer",
    snippet:
      "A little bug severity calculator I knocked together when we were figuring out a bug triage system at Casebook in 2020",
  },
  {
    title: "Coffee pacer",
    href: "coffee-pacer",
    snippet: "A timer I made for making pour-over coffee",
  },
  {
    title: "Choose your own adventure",
    href: "cyoa",
    snippet:
      "An (unfinished) experiment with making choose your own adventure stories",
  },
  {
    title: "Emoji HTML code lookup",
    href: "emoji-reference",
    snippet: "A simple lookup for Emoji HTML entities",
  },
  {
    title: "Hexle",
    href: "hexle",
    snippet: "It's like wordle, but you guess the hex code for a random color",
  },
  {
    title: "What to listen to?",
    href: "lavi-vinyl",
    snippet:
      "A little glitch app my wife and I made for randomly selecting one of our vinyl records",
  },
  {
    title: "Pour over schedule calculator",
    href: "pour-over-schedule-calculator",
    snippet: "A little calculator I made for timing pour-over coffee",
  },
  {
    title: "Sourdough converter",
    href: "sourdough-converter",
    snippet: "A simple tool to convert normal yeasted recipes to sourdough",
  },
  {
    title: "TNG Sounds",
    href: "tng-sounds",
    snippet: "A little Star Trek The Next Generation soundboard",
  },
  {
    title: "Tuning fork robot",
    href: "tuning-fork",
    snippet:
      "A simple tuning fork web app I made to experiment with the Web Audio API",
  },
  {
    title: "Turnip calc",
    href: "turnip-calc",
    snippet: "A turnip returns calculator for Animal Crossings: New Horizons",
  },
];

const sharedProjectProps: Partial<PageProps> = {
  type: "project",
  project: true,
};

const pages: PageProps[] = [
  {
    title: "Experience",
    href: "experience",
  },
  {
    title: "Personal projects",
    href: "projects",
    snippet: "Some small web projects I've done in my personal time.",
  },
  ...projects.map((p) => ({ ...p, ...sharedProjectProps })),
  {
    title: "Blog",
    href: "blog",
  },
  ...blogs.map((b) => ({ ...b, ...sharedBlogProps })),
  {
    title: "Video",
    href: "video",
  },
];

export const getPage = (href: PageProps["href"]) =>
  pages.filter((p) => p.href === href)[0];

/**
 * Defines the links that show in the nav
 */
export const navLinks = [
  {
    title: "Home",
    href: "/",
  },
  {
    title: getPage("experience").title,
    href: "/experience",
  },
  {
    title: getPage("projects").title,
    href: "/projects",
  },
  {
    title: getPage("blog").title,
    href: "/blog",
  },
  {
    title: getPage("video").title,
    href: "/video",
  },
];

export default pages;
