# Development notes

- Blog pages go in `src/pages/blog`
- Project pages go in `src/pages/projects`
- `@/` is aliased to `src/`
- Add new pages to `pageData.ts`
  - Add projects to `projects` array
  - Add blogs to `blogs` array
  - If adding new category, add it to `navLinks`
- Add new video data to `videoData.ts`
- Project uses tailwind, try to avoid any custom styling and use a tailwind class instead
- Add new components to `src/components`, new MDX components as necessary in `mdx-components.tsx` for rendering new markdown
